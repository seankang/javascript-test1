console.clear();

var arr2  = [1, [2, 3]];
var arr = [1, [2, [3], [4, 5], [6, [7, [8], [9, [10]]]]]];


var sum = 0;
function countit(mylist){
	var innerSum = 0;
	var size = mylist.length;
  for (var i =0; i < size; i++){
  	if (Array.isArray(mylist[i])) {
    	//console.log("calling recursive");
    	countit(mylist[i]);
    }
    else {
    	sum += mylist[i];
      //console.log(sum);
    }
  }
}
countit(arr);
console.log(sum);
